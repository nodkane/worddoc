﻿##This file is not part of the module it is only to assist with creating the release 


#region variables
$powershellgallaryapi
$Name            = "worddoc"
$SourceTree      = "C:\Users\shaneh\Documents\SourceTree\$name\"
$Development     = "C:\Users\shaneh\Documents\SourceTree\$name\Development"
$Release         = "C:\Users\shaneh\Documents\SourceTree\$name\Release"
$ModuleFolder    = "$env:USERPROFILE\Documents\WindowsPowerShell\Modules\$name\1.0.1.1"
#endregion

#Remove Existing Module
Remove-Item -path $ModuleFolder -Force -Recurse

#Create New Module
New-Item -Path $ModuleFolder -ItemType Directory -ErrorAction SilentlyContinue
Copy-Item -path $Development\WordDoc.psm1 -Destination $ModuleFolder 
Copy-Item -path $Development\WordDoc.psd1 -Destination $ModuleFolder
Copy-Item -path $SourceTree\license.txt   -Destination $ModuleFolder
Import-Module worddoc -force
#endregion

pause

#region Publish
#Publish-Module -Name WordDoc -NuGetApiKey $NuGetApiKey

#region Document 
Import-Module -Name $name -Force
Add-Type -AssemblyName Microsoft.Office.Interop.Word

$HTMLdoc = @{
  'moduleName'        = "$Name"
  'template'          = "pshome:\Scripts\psDoc\out-html-template.ps1"
  'outputDir'         = "$ReleaseFolder"
  'fileName'          = "$Name.html"
}

$MDdoc = @{
  'moduleName'        = "$Name"
  'template'          = "pshome:\Scripts\psDoc\out-markdown-template.ps1"
  'outputDir'         = "$ReleaseFolder"
  'fileName'          = "$Name.md"
}

. pshome:\Scripts\psDoc\psDoc.ps1 @HTMLDoc
. pshome:\Scripts\psDoc\psDoc.ps1 @MDDoc
#endregion


Remove-Module -Name $Name


