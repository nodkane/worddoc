﻿#requires -version 3.0
#requires -module WordDoc
 <#
      This example shows how you can quickly and effortless create a world document from powershell.

      import-module -name WordDoc
      .\example.ps1
      
      Author 
      Shane Hoey 
      
      Project URL
      https://bitbucket.org/shanehoey/worddoc
  #>

Import-Module -Name Worddoc -Force

$a = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et urna eget lectus rhoncus molestie rutrum in diam. Etiam quis convallis risus. Phasellus quis viverra nulla. Etiam non eleifend enim. Fusce dictum euismod mauris, sit amet pulvinar tellus iaculis vitae. Suspendisse finibus lobortis consequat. Morbi a lobortis libero. Etiam est orci, facilisis ac lacus et, maximus aliquam elit. Nulla sed nisl a elit convallis pulvinar ultrices eget urna.'
$b = 'Mauris quis mattis lorem. Curabitur interdum commodo velit non interdum. Morbi auctor purus vel enim consectetur tempor. Nunc non nisl in felis blandit porta. Donec pellentesque felis id diam semper, ac egestas lectus ullamcorper. Aliquam feugiat purus eget quam elementum, ac viverra tortor elementum. Fusce tincidunt et purus quis sollicitudin. Aliquam gravida vel leo et posuere. Aenean rhoncus ante nec sapien semper, at tempus tellus dictum. Pellentesque risus risus, facilisis sit amet metus rutrum, semper lobortis orci. Quisque viverra, tellus nec pulvinar rhoncus, tortor massa faucibus lorem, ut ullamcorper mi mi nec dui. Aliquam id nulla eget nunc aliquet mattis vitae ut risus.'

$c = Get-Service | Where-Object status -eq stopped | Where-Object name -like p*
$d = Get-Process | Where-Object name -like p* | Select-Object -Property name,id

$wi = New-WordInstance -Visable $true -Verbose 
$wd = New-WordDocument -WordInstance $wi 

Add-WordCoverPage -CoverPage Banded -WordInstance $wi -WordDoc $wd
Add-WordBreak -breaktype NewPage -WordInstance $wi -WordDoc $wd
Add-WordText -text 'Table of Contents' -WDBuiltinStyle wdStyleTitle -WordDoc $wd
Add-WordTOC -Word $wi -WordDoc $wd
Add-WordBreak -breaktype NewPage -WordInstance $wi -WordDoc $wd
Add-WordText -text 'Heading1' -WDBuiltinStyle wdStyleHeading1 -WordDoc $wd
Add-WordText -text $a -WDBuiltinStyle wdStyleNormal -WordDoc $wd
Add-WordText -text 'Heading2' -WDBuiltinStyle wdStyleHeading2 -WordDoc $wd
Add-WordText -text $b -WDBuiltinStyle wdStyleNormal -WordDoc $wd
Add-WordText -text 'Heading3' -WDBuiltinStyle wdStyleHeading3 -WordDoc $wd
Add-WordText -text $a -WDBuiltinStyle wdStyleNormal -WordDoc $wd
Add-WordText -text 'Heading4' -WDBuiltinStyle wdStyleHeading4 -WordDoc $wd
Add-WordText -text $b -WDBuiltinStyle wdStyleNormal -WordDoc $wd
Add-WordText -text 'Heading5' -WDBuiltinStyle wdStyleHeading5 -WordDoc $wd 
Add-WordText -text $a -WDBuiltinStyle wdStyleNormal -WordDoc $wd
Add-WordText -text 'Heading6' -WDBuiltinStyle wdStyleHeading6 -WordDoc $wd
Add-WordText -text $b -WDBuiltinStyle wdStyleNormal -WordDoc $wd
Add-WordText -text 'Bullet' -WDBuiltinStyle wdStyleListBullet -WordDoc $wd
Add-WordText -text 'Bullet' -WDBuiltinStyle wdStyleListBullet -WordDoc $wd


Add-WordTable -Object $d -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -GridTable 'Grid Table 5 Dark' -GridAccent 'Accent 5' -FirstColumn $false -WordDoc $wd
Add-WordTable -Object $d -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -GridTable 'Grid Table 5 Dark' -GridAccent 'Accent 5' -VerticleTable -HeaderRow $false -WordDoc $wd
Add-WordBreak -breaktype NewPage -WordInstance $wi -WordDoc $wd
Add-WordBreak -breaktype Section -WordInstance $wi -WordDoc $wd
Add-WordText -text title -WDBuiltinStyle wdStyleHeading1 -WordDoc $wd
Set-WordOrientation -Orientation Landscape -WordInstance $wi

Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceName,status) -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -GridTable 'Grid Table 4' -GridAccent 'Accent 5' -WordDoc $wd

Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormat3DEffects1
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormat3DEffects2
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormat3DEffects3
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatClassic1
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatClassic2
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatClassic3
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatClassic4
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatColorful1
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatColorful2
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatColorful3
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatColumns1
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatColumns2
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatColumns3
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatColumns4
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatColumns5
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatContemporary
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatElegant
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatGrid1
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatGrid2
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatGrid3
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatGrid4
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatGrid5
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatGrid6
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatGrid7
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatGrid8
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatList1
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatList2
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatList3
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatList4
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatList5
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatList6
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatList7
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatList8
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatNone
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatProfessional
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatSimple1
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatSimple2
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatSimple3
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatSubtle1
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatSubtle2
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatWeb1
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatWeb2
Add-WordTable -Object ($c | Select-Object -Property Displayname,ServiceNAme,status) -WordDoc $wd -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -WDTableFormat wdTableFormatWeb3

Add-WordBreak -breaktype Section -WordInstance $wi -WordDoc $wd
Add-WordBreak -breaktype NewPage -WordInstance $wi -WordDoc $wd

Set-WordOrientation -Orientation Portrait -WordInstance $wi
Set-WordBuiltInProperty -WdBuiltInProperty wdPropertyTitle   -Text "Title"      -WordDoc $wd
Set-WordBuiltInProperty -WdBuiltInProperty wdPropertyCompany -Text "Company"    -WordDoc $wd
Set-WordBuiltInProperty -WdBuiltInProperty wdPropertyAuthor  -Text "Shane Hoey" -WordDoc $wd
Update-WordTOC -WordDoc $wd 
#Save-WordDocument -WordSaveFormat wdFormatHTML -filename scripthtml -folder C:\test
#Save-WordDocument -WordSaveFormat wdFormatPDF -filename scriptpdf -folder C:\test
#Save-WordDocument -WordSaveFormat wdFormatDocument -filename scriptdoc -folder C:\test
Close-WordDocument -Word $wi -WordDoc $wd