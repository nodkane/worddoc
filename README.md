# WordDoc - Create Word Documents direct from PowerShell #
This project is hosted on bitbucket https://bitbucket.org/shanehoey/worddoc

WordDoc helps you create documents directly from powershell. This simple module enables your to quickly and effortlessly create Word Documents directly from Powershell. 


**Please Note:** This is an early release of the module, please report bugs via bitbucket site.  Also there is currently no 
comment based help.

### Prerequistes ###
Microsoft Office must be installed the computer you are running the script from.

### Save Latest release from PowerShell Gallery (Powershell v5)###
 1. Save latest released version directly from PowerShell Gallary 

```
#!powershell
save-module -modulename WordDoc -path <path>
```

### Install latest release from PowerShell Gallery (Powershell v5)###
 1. Install latest released version directly from PowerShell Gallary 

```
#!powershell
install-module -modulename WordDoc
```

 2. download the Example.ps1 document, and start creating your own word documents from PowerShell :)

### Installation (PowerShell v3 & v4)###
 1. Copy the worddoc.psm1 file to a WordDoc folder into one of the following folders

```
#!powershell

%userprofile%\Documents\WindowsPowerShell\Modules\WordDoc
%windir%\System32\WindowsPowerShell\v1.0\Modules\WordDoc
 
```
 2. download the Example.ps1 document, and start creating your own word documents from PowerShell :)
### Example Usage (Module)###

```
#!powershell

Import-Module Worddoc 
$a = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' 
$word = Invoke-Word 
$worddoc = New-WordDocument -Word $word 
Add-WordCoverPage -CoverPage Banded -Word $word -WordDoc $worddoc 
Add-WordText -text 'Table of Contents' -WDBuiltinStyle wdStyleTitle -WordDoc $worddoc 
Add-WordTOC -word $word -WordDoc $worddoc 
Add-WordBreak -breaktype NewPage -word $word -WordDoc $worddoc 
Add-WordText -text 'Heading1' -WDBuiltinStyle wdStyleHeading1 -WordDoc $worddoc
Add-WordText -text $a -WDBuiltinStyle wdStyleNormal -WordDoc $worddoc 

```

### Todo list before I release v1  ###
* Comment Based Help 

### Credit and Thanks ###
The massive thanks to the following people for blog there own scripts that have helped me write this module

* Laerte Junior  -    https://www.simple-talk.com/sql/database-administration/automating-your-sql-server-best-practice-reports-the-document/
   Laerte's blog post helped me to create my own add table function, there are lots of similarities between the two functions and full credit goes to Laerte for this function.  

* Boe Prox - https://learn-powershell.net/2014/12/31/beginning-with-powershell-and-word/
   Boe's blog pose help me to work out the basis of working with Powershell and Word, especially how to use [Microsoft.Office.Interop.Word] 

* Chase Floreell  - https://github.com/ChaseFlorell/psDoc/
   Chase wrote a great script to create html and md files based to document all the help files. 
   https://github.com/ChaseFlorell/psDoc/