﻿Import-Module Worddoc 
$a = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' 
$word = Invoke-Word 
$worddoc = New-WordDocument -Word $word 
Add-WordCoverPage -CoverPage Banded -Word $word -WordDoc $worddoc 
Add-WordText -text 'Table of Contents' -WDBuiltinStyle wdStyleTitle -WordDoc $worddoc 
Add-WordTOC -word $word -WordDoc $worddoc 
Add-WordBreak -breaktype NewPage -word $word -WordDoc $worddoc 
Add-WordText -text 'Heading1' -WDBuiltinStyle wdStyleHeading1 -WordDoc $worddoc
Add-WordText -text $a -WDBuiltinStyle wdStyleNormal -WordDoc $worddoc 