﻿#requires -version 3.0

 <#
      .SYNOPSIS
      WordDoc helps you quickly generate Word Documents from PowerShell quickly and effortless.

      .DESCRIPTION
      WordDoc helps you quickly generate Word Documents from PowerShell quickly and effortless.
      
      .EXAMPLE
      import-module -name WordDoc
      Imports the WordDOc module into the current Powershell Instance

      .NOTES
        Author Shane Hoey shanehoey.com 

      Credits
        Boe Prox 
        Laerte Junior
        Full Credit list is maintaned on the Project sige

      .LINK
      Project URL
      https://bitbucket.org/shanehoey/worddoc

  #>

Function Invoke-Word 
{
  <#
      .SYNOPSIS
      Describe purpose of "Invoke-Word" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER Visable
      Include parameter -Visable $true to  Display word or -Visable $false to keep it hidden

      .EXAMPLE
      Invoke-Word -Visable Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Invoke-Word

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  [CmdletBinding( SupportsShouldProcess = $true)]
  Param( 
    [Parameter(Mandatory=$false,Position = 0,HelpMessage = 'Display word or keep it hidden')]
    [bool]$Visable = $true
  )
  
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
    try 
    { 
      #Required for Word assembly
      Add-Type -AssemblyName Microsoft.Office.Interop.Word -ErrorAction SilentlyContinue
    }
    catch
    {
      Write-Warning  -Message "$($MyInvocation.InvocationName) - Unable to add Word Assembly, Word must be installed for this module... exiting"
      break
    }

    try 
    {
      $Word = New-Object -ComObject Word.Application
      $Word.Visible = $Visable
      return $Word
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - Unable to open word... exiting"
      break
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function New-WordDocument 
{
  <#
      .SYNOPSIS
      Describe purpose of "New-WordDocument" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER Word
      Describe parameter -Word.

      .EXAMPLE
      New-WordDocument -Word Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online New-WordDocument

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


   
  [CmdletBinding( SupportsShouldProcess = $true)]
  Param( 
    [Parameter(Mandatory=$true,Position = 0,HelpMessage = 'Add help message for user')]
    $Word
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 

    try 
    {
      $WordDoc = $Word.Documents.Add()
      $WordDoc.Activate()
      return $WordDoc
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Save-WordDocument 
{
  <#
      .SYNOPSIS
      Describe purpose of "Save-WordDocument" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER WordSaveFormat
      Describe parameter -WordSaveFormat.

      .PARAMETER filename
      Describe parameter -filename.

      .PARAMETER folder
      Describe parameter -folder.

      .PARAMETER WordDoc
      Describe parameter -WordDoc.

      .EXAMPLE
      Save-WordDocument -WordSaveFormat Value -filename Value -folder Value -WordDoc Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Save-WordDocument

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  [CmdletBinding( SupportsShouldProcess = $true)]
  Param( 
    [Parameter(Mandatory = $true,HelpMessage = 'Add help message for user',Position = 0)]
    [Microsoft.Office.Interop.Word.WdSaveFormat]$WordSaveFormat,
    [Parameter(Mandatory = $true,HelpMessage = 'Add help message for user',Position = 1)]
    [string]$filename,
    [Parameter(Mandatory = $true,HelpMessage = 'Add help message for user')]
    [String]$folder,
    [Parameter(Mandatory = $true,HelpMessage = 'Add help message for user')]
    $WordDoc
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
    try 
    {
      if ($PSBoundParameters.ContainsKey('folder')) 
      {
        $filename = Join-Path -Path $folder -ChildPath $filename
      }
      $WordDoc.SaveAs([ref]($filename) ,$WordSaveFormat)
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Close-WordDocument 
{
  <#
      .SYNOPSIS
      Describe purpose of "Close-WordDocument" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER Word
      Describe parameter -Word.

      .PARAMETER WordDoc
      Describe parameter -WordDoc.

      .EXAMPLE
      Close-WordDocument -Word Value -WordDoc Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Close-WordDocument

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Mandatory = $true)]
    $Word,
    [Parameter(Mandatory = $true)]
    $WordDoc
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
      
    try
    {
      $WordDoc.Close() 
      $Word.Quit()  
    }
    catch
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordText 
{
  <#
      .SYNOPSIS
      Describe purpose of "Add-WordText" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER text
      Describe parameter -text.

      .PARAMETER WdColor
      Describe parameter -WdColor.

      .PARAMETER WDBuiltinStyle
      Describe parameter -WDBuiltinStyle.

      .PARAMETER WordDoc
      Describe parameter -WordDoc.

      .EXAMPLE
      Add-WordText -text Value -WdColor Value -WDBuiltinStyle Value -WordDoc Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Add-WordText

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Position = 0,HelpMessage = 'Add help message for user',
    Mandatory = $true)] 
    [String]$text,
    [Parameter(Position = 1,
    Mandatory = $false)] 
    [Microsoft.Office.Interop.Word.WdColor]$WdColor,
    [Parameter(Position = 2)] 
    [Microsoft.Office.Interop.Word.WdBuiltinStyle]$WDBuiltinStyle,
    [Parameter(Mandatory = $true)]
    $WordDoc
  )
  Begin
  {
      Add-Type -AssemblyName Microsoft.Office.Interop.Word
      Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" 
  }
  Process { 
  
    try
    {
      if ($PSBoundParameters.ContainsKey('Color')) 
      {
        $WordDoc.Application.Selection.font.Color = $WdColor
      }
      if ($PSBoundParameters.ContainsKey('WDBuiltinStyle')) 
      {
        $WordDoc.application.selection.Style = $WDBuiltinStyle
      }
           
      $WordDoc.Application.Selection.TypeText("$($text)")    
      $WordDoc.Application.Selection.TypeParagraph() 
      $WordDoc.application.selection.Style = [Microsoft.Office.Interop.Word.WdBuiltinStyle]'wdStyleNormal'
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordBreak 
{
  <#
      .SYNOPSIS
      Describe purpose of "Add-WordBreak" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER breaktype
      Describe parameter -breaktype.

      .PARAMETER WordDoc
      Describe parameter -WordDoc.

      .EXAMPLE
      Add-WordBreak -breaktype Value -WordDoc Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Add-WordBreak

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>
  [CmdletBinding( SupportsShouldProcess = $true)]
  param (
    [Parameter(Position = 0, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'GridTable')]
    [ValidateSet('NewPage', 'Section','Paragraph')]
    [string]$breaktype,
    [Parameter(Mandatory = $true)]
    $WordDoc,
    [Parameter(Mandatory = $true)]
    $Word
  )
 
  Begin { Add-Type -AssemblyName Microsoft.Office.Interop.Word
  Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
 
    try
    {  
      switch ($breaktype)
      {
        'NewPage' 
        {
          $Word.Selection.InsertNewPage() 
        }
        'Section' 
        {
          $Word.Selection.Sections.Add()  
        }
        'Paragraph' 
        {
          $Word.Selection.InsertParagraph() 
        }
      }
      [Void]$WordDoc.application.selection.goto([Microsoft.Office.Interop.Word.WdGoToItem]::wdGoToBookmark,$null,$null,'\EndOfDoc')
    
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Set-WordBuiltInProperty 
{
  <#
      .SYNOPSIS
      Describe purpose of "Set-WordBuiltInProperty" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER WdBuiltInProperty
      Describe parameter -WdBuiltInProperty.

      .PARAMETER text
      Describe parameter -text.

      .PARAMETER WordDoc
      Describe parameter -WordDoc.

      .EXAMPLE
      Set-WordBuiltInProperty -WdBuiltInProperty Value -text Value -WordDoc Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Set-WordBuiltInProperty

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  

  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    
    [Parameter(Position = 0,HelpMessage = 'Add help message for user',Mandatory = $true)] 
    [Microsoft.Office.Interop.Word.WdBuiltInProperty]$WdBuiltInProperty,
    [Parameter(Position = 1,HelpMessage = 'Add help message for user',mandatory = $true)] 
    [String]$text,
    [Parameter(Mandatory = $true)]
    $WordDoc
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
 
    try
    { 
      Write-Verbose -Message $WdBuiltInProperty
      $WordDoc.BuiltInDocumentProperties.item($WdBuiltInProperty).value = $text
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordCoverPage 
{
  <#
      .SYNOPSIS
      Describe purpose of "Add-WordCoverPage" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER CoverPage
      Describe parameter -CoverPage.

      .PARAMETER Word
      Describe parameter -Word.

      .PARAMETER WordDoc
      Describe parameter -WordDoc.

      .EXAMPLE
      Add-WordCoverPage -CoverPage Value -Word Value -WordDoc Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Add-WordCoverPage

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  

  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Position = 0)] 
    [ValidateSet('Austin', 'Banded','Facet','Filigree','Grid','Integral','Ion (Dark)','Ion (Light)','Motion','Retrospect','Semaphore','Sideline','Slice (Dark)','Slice (Light)','Viewmaster','Whisp')]  
    [string]$CoverPage,
    [Parameter(Mandatory = $true)]
    $Word ,
    [Parameter(Mandatory = $true)]
    $WordDoc 
  )  
  Begin { Add-Type -AssemblyName Microsoft.Office.Interop.Word
  Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 

    try
    {
      $Selection = $WordDoc.application.selection
    
      $Word.Templates.LoadBuildingBlocks()
      $bb = $Word.templates | Where-Object -Property name -EQ -Value 'Built-In Building Blocks.dotx'
      $part = $bb.BuildingBlockEntries.item($CoverPage)
      $null = $part.Insert($Word.Selection.range,$true) 
  
      [Void]$Selection.goto([Microsoft.Office.Interop.Word.WdGoToItem]::wdGoToBookmark,$null,$null,'\EndOfDoc')
    }
    catch
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Set-WordOrientation
{
  <#
      .SYNOPSIS
      Describe purpose of "Set-WordOrientation" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER Orientation
      Describe parameter -Orientation.

      .PARAMETER Word
      Describe parameter -Word.

      .EXAMPLE
      Set-WordOrientation -Orientation Value -Word Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Set-WordOrientation

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


 

  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Position = 0,HelpMessage = 'Add help message for user',Mandatory = $true)] 
    [ValidateSet('Portrait', 'Landscape')]  
    [string]$Orientation,
    [Parameter(Mandatory = $true)]
    $Word 
            
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
 
    try 
    {
      switch ($Orientation)
      {
        'Portrait'  
        {
          $Word.Selection.PageSetup.Orientation = 0 
        }
        'Landscape'  
        {
          $Word.Selection.PageSetup.Orientation = 1 
        }    
      }
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordTOC 
{
  <#
      .SYNOPSIS
      Describe purpose of "Add-WordTOC" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER Word
      Describe parameter -Word.

      .PARAMETER WordDoc
      Describe parameter -WordDoc.

      .EXAMPLE
      Add-WordTOC -Word Value -WordDoc Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Add-WordTOC

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  [CmdletBinding( SupportsShouldProcess = $true)]  
  param (
    [Parameter(Mandatory = $true)]
    $Word ,
    [Parameter(Mandatory = $true)]
    $WordDoc 
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
   
    try 
    {
      $toc = $WordDoc.TablesOfContents.Add($Word.selection.Range)
      $toc.TabLeader = 0
      $toc.HeadingStyles 
      $WordDoc.Application.Selection.TypeParagraph()
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Update-WordTOC 
{
  <#
      .SYNOPSIS
      Describe purpose of "Update-WordTOC" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER WordDoc
      Describe parameter -WordDoc.

      .EXAMPLE
      Update-WordTOC -WordDoc Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Update-WordTOC

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  [CmdletBinding( SupportsShouldProcess = $true)]   
  param (
    [Parameter(Mandatory = $true)]
    $WordDoc 
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 

    try 
    {
      $WordDoc.Fields | 
      ForEach-Object -Process {
        $_.Update() 
      }
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordTable 
{
  <#
      .SYNOPSIS
      Describe purpose of "Add-WordTable" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER Object
      Describe parameter -Object.

      .PARAMETER WdAutoFitBehavior
      Describe parameter -WdAutoFitBehavior.

      .PARAMETER WdDefaultTableBehavior
      Describe parameter -WdDefaultTableBehavior.

      .PARAMETER HeaderRow
      Describe parameter -HeaderRow.

      .PARAMETER TotalRow
      Describe parameter -TotalRow.

      .PARAMETER BandedRow
      Describe parameter -BandedRow.

      .PARAMETER FirstColumn
      Describe parameter -FirstColumn.

      .PARAMETER LastColumn
      Describe parameter -LastColumn.

      .PARAMETER BandedColumn
      Describe parameter -BandedColumn.

      .PARAMETER PlainTable
      Describe parameter -PlainTable.

      .PARAMETER GridTable
      Describe parameter -GridTable.

      .PARAMETER ListTable
      Describe parameter -ListTable.

      .PARAMETER ListAccent
      Describe parameter -ListAccent.

      .PARAMETER GridAccent
      Describe parameter -GridAccent.

      .PARAMETER RemoveProperties
      Describe parameter -RemoveProperties.

      .PARAMETER VerticleTable
      Describe parameter -VerticleTable.

      .PARAMETER NoParagraph
      Describe parameter -NoParagraph.

      .PARAMETER WordDoc
      Describe parameter -WordDoc.

      .EXAMPLE
      Add-WordTable -Object Value -WdAutoFitBehavior Value -WdDefaultTableBehavior Value -HeaderRow Value -TotalRow Value -BandedRow Value -FirstColumn Value -LastColumn Value -BandedColumn Value -PlainTable Value -GridTable Value -ListTable Value -ListAccent Value -GridAccent Value -RemoveProperties -VerticleTable -NoParagraph -WordDoc Value
      Describe what this call does

      .NOTES
      Place additional notes here.
      
      ***Credit***
      Laerte Junior  - https://www.simple-talk.com/sql/database-administration/automating-your-sql-server-best-practice-reports-the-document/

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Add-WordTable

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Position = 0,HelpMessage = 'psobject to send to word', Mandatory = $true, ValuefromPipeline = $true)]    
    [psobject]$Object,
    [Parameter(Position = 1)] 
    [Microsoft.Office.Interop.Word.WdAutoFitBehavior]$WdAutoFitBehavior = 'wdAutoFitContent',
    [Parameter(Position = 2)] 
    [Microsoft.Office.Interop.Word.WdDefaultTableBehavior]$WdDefaultTableBehavior = 'wdWord9TableBehavior', 
    [Parameter(Position = 3)]
    [bool]$HeaderRow = $true,
    [bool]$TotalRow = $false,
    [bool]$BandedRow = $true,
    [bool]$FirstColumn = $false,
    [bool]$LastColumn = $false,
    [bool]$BandedColumn = $false,
    [Parameter(Position = 4, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'PlainTable')]
    [ValidateSet('Table Grid', 'Table Grid Light','Plain Table 1','Plain Table 2','Plain Table 3','Plain Table 4','Plain Table 5')]
    [string]$PlainTable,
    [Parameter(Position = 5, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'GridTable')]
    [ValidateSet('Grid Table 1 Light', 'Grid Table 2','Grid Table 3','Grid Table 4','Grid Table 5 Dark','Grid Table 6 Colorful','Grid Table 7 Colorful')]
    [string]$GridTable,
    [Parameter(Position = 6, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'ListTable')]
    [ValidateSet('List Table 1 Light', 'List Table 2','List Table 3','List Table 4','List Table 5 Dark','List Table 6 Colorful','List Table 7 Colorful')]
    [string]$ListTable,
    [Parameter(Position = 7, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'ListTable')]
    [ValidateSet('Accent 1', 'Accent 2','Accent 3','Accent 4','Accent 5','Accent 6')]
    [string]$ListAccent,
    [Parameter(Position = 8, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'GridTable')]
    [ValidateSet('Accent 1', 'Accent 2','Accent 3','Accent 4','Accent 5','Accent 6')]
    [string]$GridAccent,
    [switch]$RemoveProperties,
    [switch]$VerticleTable,
    [switch]$NoParagraph,
    [Parameter(Mandatory = $true)]
    $WordDoc
  )
   
   Begin { Add-Type -AssemblyName Microsoft.Office.Interop.Word
  Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
    try 
    {
      $TableRange = $WordDoc.application.selection.range
      
      if (!($VerticleTable)) {
        $Columns = @($Object | Get-Member -MemberType Property, NoteProperty).count
        if($RemoveProperties) { $Rows = @($Object).count } 
        else {$Rows = @($Object).count +1 }
      }
      if ($VerticleTable) {
        if($RemoveProperties) { $Columns = @($Object).count } 
        else {$Columns = @($Object).count +1 }
        $Rows = @($Object | Get-Member -MemberType Property, NoteProperty).count
      }
 
      $Table = $WordDoc.Tables.Add($TableRange, $Rows, $Columns,$WdDefaultTableBehavior,$WdAutoFitBehavior) 
 
      if ($PSBoundParameters.ContainsKey('PlainTable')){ $Table.style = $PlainTable } 
      if ($PSBoundParameters.ContainsKey('GridTable')) { 
        if($PSBoundParameters.ContainsKey('GridAccent'))
        {
          $Table.style = ($GridTable + ' - ' + $GridAccent) 
        }
        else 
        {
          $Table.style = $GridTable 
        } 
      } 
      if ($PSBoundParameters.ContainsKey('ListTable')) {
        if($PSBoundParameters.ContainsKey('ListAccent'))
        {$Table.style = ($ListTable + ' - ' + $ListAccent)}
        else 
        {$Table.style = $ListTable } 
      }  
      if ($PSBoundParameters.ContainsKey('HeaderRow'))    
      {
        if ($HeaderRow) 
        { $Table.ApplyStyleHeadingRows = $true }
        else 
        {$Table.ApplyStyleHeadingRows = $false } 
      }
      if ($PSBoundParameters.ContainsKey('TotalRow'))     
      {
        if ($TotalRow) 
        {
          $Table.ApplyStyleLastRow = $true 
        }
        else 
        {
          $Table.ApplyStyleLastRow = $false
        } 
      }
      if ($PSBoundParameters.ContainsKey('BandedRow'))    
      {
        if ($BandedRow) 
        {
          $Table.ApplyStyleRowBands = $true 
        }
        else 
        {
          $Table.ApplyStyleRowBands = $false
        } 
      }
      if ($PSBoundParameters.ContainsKey('FirstColumn'))  
      {
        if ($FirstColumn) 
        {
          $Table.ApplyStyleFirstColumn = $true 
        }
        else 
        {
          $Table.ApplyStyleFirstColumn = $false
        } 
      }
      if ($PSBoundParameters.ContainsKey('LastColumn'))   
      {
        if ($LastColumn) 
        {
          $Table.ApplyStyleLastColumn = $true 
        }
        else 
        {
          $Table.ApplyStyleLastColumn = $false
        } 
      }
      if ($PSBoundParameters.ContainsKey('BandedColumn')) 
      {
        if($BandedColumn) 
        {
          $Table.ApplyStyleColumnBands = $true 
        }
        else 
        {
          $Table.ApplyStyleColumnBands = $false
        } 
      }

      [int]$Row = 1
      [int]$Col = 1
    
      $PropertyNames = @()

    
      if ($Object -is [Array]){[ARRAY]$HeaderNames = $Object[0].psobject.properties | ForEach-Object -Process { $_.Name }} 
      else { [ARRAY]$HeaderNames = $Object.psobject.properties | ForEach-Object -Process { $_.Name } }
   
      if($RemoveProperties) { $Table.ApplyStyleHeadingRows = $false }
     
      if (!($VerticleTable)) {
        for ($i = 0; $i -le $Columns -1; $i++) 
        {
          $PropertyNames += $HeaderNames[$i]
          if(!$RemoveProperties) 
          {
            $Table.Cell($Row,$Col).Range.Text = $HeaderNames[$i]
          }
          $Col++
        }    
    
        if(!$RemoveProperties)
        { $Row = 2 }
   
        $Object | 
        ForEach-Object -Process {
          $Col = 1
          for ($i = 0; $i -le $Columns -1; $i++) 
          {      
            $Table.Cell($Row,$Col).Range.Text = (($_."$($PropertyNames[$i])") -as [System.string])
            $Col++
          }    
          $Row++
        }
      } 

      if ($VerticleTable) {
        for ($i = 0; $i -le $Rows -1; $i++) 
        {
          $PropertyNames += $HeaderNames[$i]
          if(!$RemoveProperties) 
          {
            $Table.Cell($Row,$Col).Range.Text = $HeaderNames[$i]
          }
          $Row++
        }    
    
        if(!$RemoveProperties)
        { $Col = 2 }
   
        $Object | 
        ForEach-Object -Process {
          $Row = 1
          for ($i = 0; $i -le $Rows -1; $i++) 
          {      
            $Table.Cell($Row,$Col).Range.Text = (($_."$($PropertyNames[$i])") -as [System.string])
            $Row++
          }    
          $Col++
        }
      }
  
      $Selection = $WordDoc.application.selection
      [Void]$Selection.goto([Microsoft.Office.Interop.Word.WdGoToItem]::wdGoToBookmark,$null,$null,'\EndOfDoc')
      
      if(!($NoParagraph)) { $WordDoc.Application.Selection.TypeParagraph() }
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }

  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Get-WordStyle 
{
  <#
      .SYNOPSIS
      Describe purpose of "Get-WordStyle" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .EXAMPLE
      Get-WordStyle
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Get-WordStyle

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  [CmdletBinding( SupportsShouldProcess = $true)]
  param()

  Begin { Add-Type -AssemblyName Microsoft.Office.Interop.Word
  Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
  
    try 
    {
      [Enum]::GetNames([Microsoft.Office.Interop.Word.WdBuiltinStyle]) |
      ForEach-Object -Process {[pscustomobject]@{
          Style = $_
      } }
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordTemplate 
{
  <#
      .SYNOPSIS
      Describe purpose of "Add-WordTemplate" in 1-2 sentences.

      .DESCRIPTION
      Add a more complete description of what the function does.

      .PARAMETER filename
      Describe parameter -filename.

      .PARAMETER WordDoc
      Describe parameter -WordDoc.

      .EXAMPLE
      Add-WordTemplate -filename Value -WordDoc Value
      Describe what this call does

      .NOTES
      Place additional notes here.

      .LINK
      URLs to related sites
      The first link is opened by Get-Help -Online Add-WordTemplate

      .INPUTS
      List of input types that are accepted by this function.

      .OUTPUTS
      List of output types produced by this function.
  #>


  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Mandatory = $true,HelpMessage = 'Add word document or template to import',Position = 0)]
    [ValidateScript({ Test-Path -Path $_ })] 
    [string]$filename,
    [Parameter(Mandatory = $true)]
    $WordDoc   
  )   
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
    
    try 
    {
      $WordDoc.Application.Selection.InsertFile([ref]($filename))
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}
  
Export-ModuleMember -Function * -Alias * -Variable *