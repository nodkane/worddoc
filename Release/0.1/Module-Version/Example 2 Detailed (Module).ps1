﻿#requires -version 3.0
#requires -module WordDoc
 <#
      This example shows how you can quickly and effortless create a world document from powershell.

      import-module -name WordDoc
      .\example.ps1
      
      Author 
      Shane Hoey 
      
      Project URL
      https://bitbucket.org/shanehoey/worddoc
  #>

Import-Module Worddoc -Force

$a = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et urna eget lectus rhoncus molestie rutrum in diam. Etiam quis convallis risus. Phasellus quis viverra nulla. Etiam non eleifend enim. Fusce dictum euismod mauris, sit amet pulvinar tellus iaculis vitae. Suspendisse finibus lobortis consequat. Morbi a lobortis libero. Etiam est orci, facilisis ac lacus et, maximus aliquam elit. Nulla sed nisl a elit convallis pulvinar ultrices eget urna.'
$b = 'Mauris quis mattis lorem. Curabitur interdum commodo velit non interdum. Morbi auctor purus vel enim consectetur tempor. Nunc non nisl in felis blandit porta. Donec pellentesque felis id diam semper, ac egestas lectus ullamcorper. Aliquam feugiat purus eget quam elementum, ac viverra tortor elementum. Fusce tincidunt et purus quis sollicitudin. Aliquam gravida vel leo et posuere. Aenean rhoncus ante nec sapien semper, at tempus tellus dictum. Pellentesque risus risus, facilisis sit amet metus rutrum, semper lobortis orci. Quisque viverra, tellus nec pulvinar rhoncus, tortor massa faucibus lorem, ut ullamcorper mi mi nec dui. Aliquam id nulla eget nunc aliquet mattis vitae ut risus.'

$c = Get-Service | Where-Object status -eq stopped | Where-Object name -like p*
$d = Get-Process | Where-Object name -like p* | Select-Object name,id

$word = Invoke-Word
$worddoc = New-WordDocument -Word $word

Add-WordCoverPage -CoverPage Banded -word $word -WordDoc $worddoc
Add-WordBreak -breaktype NewPage -Word $Word -WordDoc $worddoc
Add-WordText -text 'Table of Contents' -WDBuiltinStyle wdStyleTitle -WordDoc $worddoc
Add-WordTOC -Word $word -WordDoc $worddoc
Add-WordBreak -breaktype NewPage -Word $Word -WordDoc $worddoc
Add-WordText -text 'Heading1' -WDBuiltinStyle wdStyleHeading1 -WordDoc $worddoc
Add-WordText -text $a -WDBuiltinStyle wdStyleNormal -WordDoc $worddoc
Add-WordText -text 'Heading2' -WDBuiltinStyle wdStyleHeading2 -WordDoc $worddoc
Add-WordText -text $b -WDBuiltinStyle wdStyleNormal -WordDoc $worddoc
Add-WordText -text 'Heading3' -WDBuiltinStyle wdStyleHeading3 -WordDoc $worddoc
Add-WordText -text $a -WDBuiltinStyle wdStyleNormal -WordDoc $worddoc
Add-WordText -text 'Heading4' -WDBuiltinStyle wdStyleHeading4 -WordDoc $worddoc
Add-WordText -text $b -WDBuiltinStyle wdStyleNormal -WordDoc $worddoc
Add-WordText -text 'Heading5' -WDBuiltinStyle wdStyleHeading5 -WordDoc $worddoc 
Add-WordText -text $a -WDBuiltinStyle wdStyleNormal -WordDoc $worddoc
Add-WordText -text 'Heading6' -WDBuiltinStyle wdStyleHeading6 -WordDoc $worddoc
Add-WordText -text $b -WDBuiltinStyle wdStyleNormal -WordDoc $worddoc
Add-WordText -text 'Bullet' -WDBuiltinStyle wdStyleListBullet -WordDoc $worddoc
Add-WordText -text 'Bullet' -WDBuiltinStyle wdStyleListBullet -WordDoc $worddoc
Add-WordText -text 'Bullet' -WDBuiltinStyle wdStyleListBullet -WordDoc $worddoc
Add-WordTable -Object $d -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -GridTable 'Grid Table 5 Dark' -GridAccent 'Accent 5' -FirstColumn $false -WordDoc $worddoc
Add-WordTable -Object $d -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -GridTable 'Grid Table 5 Dark' -GridAccent 'Accent 5' -VerticleTable -HeaderRow $false -WordDoc $worddoc
Add-WordBreak -breaktype NewPage -Word $Word -WordDoc $worddoc
Add-WordBreak -breaktype Section -Word $Word -WordDoc $worddoc
Add-WordText -text title -WDBuiltinStyle wdStyleHeading1 -WordDoc $worddoc
Set-WordOrientation -Orientation Landscape -Word $word
Add-WordTable -Object ($c | select Displayname,ServiceNAme,status) -WdAutoFitBehavior wdAutoFitWindow -WdDefaultTableBehavior wdWord9TableBehavior  -GridTable 'Grid Table 4' -GridAccent 'Accent 5' -WordDoc $worddoc
Add-WordBreak -breaktype Section -Word $Word -WordDoc $worddoc
Add-WordBreak -breaktype NewPage -Word $Word -WordDoc $worddoc
Set-WordOrientation -Orientation Portrait -Word $word
Set-WordBuiltInProperty -WdBuiltInProperty wdPropertyTitle -Text "Title" -WordDoc $worddoc
Set-WordBuiltInProperty -WdBuiltInProperty wdPropertyCompany -Text "Company" -WordDoc $worddoc
Set-WordBuiltInProperty -WdBuiltInProperty wdPropertyAuthor -Text "Shane Hoey" -WordDoc $worddoc
Update-WordTOC -WordDoc $worddoc 
#Save-WordDocument -WordSaveFormat wdFormatHTML -filename scripthtml -folder C:\test
#Save-WordDocument -WordSaveFormat wdFormatPDF -filename scriptpdf -folder C:\test
#Save-WordDocument -WordSaveFormat wdFormatDocument -filename scriptdoc -folder C:\test
Close-WordDocument -Word $word -WordDoc $worddoc