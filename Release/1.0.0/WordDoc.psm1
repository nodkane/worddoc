﻿#requires -version 3.0

<#
    .SYNOPSIS
    WordDoc helps you quickly generate Word Documents from PowerShell quickly and effortless.

    .DESCRIPTION
    WordDoc helps you quickly generate Word Documents from PowerShell quickly and effortless.
      
    .EXAMPLE
    import-module -name WordDoc
    Imports the WordDoc module into the current Powershell Instance

    .NOTES
    Author Shane Hoey shanehoey.com 

    Credits
    Boe Prox 
    Laerte Junior
    Full Credit list is maintaned on the Project site. 

    .LINK
    Project Site
    https://bitbucket.org/shanehoey/WordDoc

#>

Function New-WordInstance 
{
  [Alias("invoke-word")] #Do not use this will be depreciated
  [CmdletBinding( SupportsShouldProcess = $true)]
  Param( 
  
    [Parameter(Mandatory=$false,Position = 0,HelpMessage = 'Add help message for user')]
    [ValidateScript({ if([boolean]!(get-variable $_ -ErrorAction SilentlyContinue)){ $true } else {throw "Variable $($_) already exists" } })]
    [string]$WordInstanceName = "WordInstance",
  
    [Parameter(Mandatory=$false,Position = 0,HelpMessage = 'Display word or keep it hidden')]
    [bool]$Visable = $true
  )
  
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
    #TODO Investigate a better way to catch these errors 
    
    try 
    { 
      #Required for Word assembly
      Add-Type -AssemblyName Microsoft.Office.Interop.Word -ErrorAction SilentlyContinue
    }
    catch
    {
      Write-Warning  -Message "$($MyInvocation.InvocationName) - Unable to add Word Assembly, Word must be installed for this module... exiting"
      break
    }

    try 
    {
      $Word = New-Object -ComObject Word.Application -Property @{Visible=$Visable} 
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - Unable to Invoke Word... exiting"
      break
    }
    
    try 
    {
      New-Variable -Name $WordInstanceName -Value $Word -Scope Global  -ErrorAction SilentlyContinue
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - Unable to create variable... exiting"
      break
    }   
  }
  
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function New-WordDocument 
{

  [CmdletBinding( SupportsShouldProcess = $true)]
  Param( 
    
    [Parameter(Mandatory=$false,Position = 0,HelpMessage = 'Add help message for user')]
    [alias("Word")]
    #Todo cast type instead ie [Microsoft.Office.Interop.Word.Application]$WordInstance but does not work
    [ValidateScript( { if($_ -is [Microsoft.Office.Interop.Word.Application]) { $True } else { throw "WordInstance was not of type [Microsoft.Office.Interop.Word.Application]" } })]
    $WordInstance = $GLOBAL:WordInstance,
 
    #Todo fix up the checj a     
    [Parameter(Mandatory=$false,Position = 1,HelpMessage = 'Add help message for user')]
    [ValidateScript({ if([boolean]!(get-variable $_ -ErrorAction SilentlyContinue)){ $true } else {throw "Variable $($_) already exists" } })]
    [String]$WordDocName = "WordDoc"
    
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 

    try 
    {
      $WordDoc = $WordInstance.Documents.Add()
      $WordDoc.Activate()
      New-Variable -Name $WordDocName -Value $WordDoc -Scope Global -ErrorAction SilentlyContinue
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Save-WordDocument 
{
 
  [CmdletBinding( SupportsShouldProcess = $true)]
  Param( 
    [Parameter(Mandatory = $true,HelpMessage = 'Add help message for user',Position = 0)]
    [Microsoft.Office.Interop.Word.WdSaveFormat]$WordSaveFormat,
    [Parameter(Mandatory = $true,HelpMessage = 'Add help message for user',Position = 1)]
    [string]$filename,
    [Parameter(Mandatory = $true,HelpMessage = 'Add help message for user')]
    [String]$folder,
    [Parameter(Mandatory = $true,HelpMessage = 'Add help message for user')]
    $WordDoc
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
    try 
    {
      if ($PSBoundParameters.ContainsKey('folder')) 
      {
        $filename = Join-Path -Path $folder -ChildPath $filename
      }
      $WordDoc.SaveAs([ref]($filename) ,$WordSaveFormat)
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Close-WordDocument 
{

  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Mandatory = $true)]
    [alias("Word")]
    #Todo cast type instead ie [Microsoft.Office.Interop.Word.Application]$WordInstance but does not work
    [ValidateScript( { if($_ -is [Microsoft.Office.Interop.Word.Application]) { $True } else { throw "WordInstance was not of type [Microsoft.Office.Interop.Word.Application]" } })]
    $WordInstance,
    [Parameter(Mandatory = $true)]
    $WordDoc
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
      
    try
    {
      $WordDoc.Close() 
      $WordInstance.Quit()  
    }
    catch
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordText 
{

  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Position = 0,HelpMessage = 'Add help message for user',
    Mandatory = $true)] 
    [String]$text,
    
    [Parameter(Position = 1,
    Mandatory = $false)] 
    [Microsoft.Office.Interop.Word.WdColor]$WdColor,
    
    [Parameter(Position = 2)] 
    [Microsoft.Office.Interop.Word.WdBuiltinStyle]$WDBuiltinStyle,
    
    [Parameter(Mandatory = $false)]
    $WordDoc = $Global:WordDoc
  )
  Begin
  {
    Add-Type -AssemblyName Microsoft.Office.Interop.Word
    Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" 
  }
  Process { 
  
    try
    {
      if ($PSBoundParameters.ContainsKey('Color')) 
      {
        $WordDoc.Application.Selection.font.Color = $WdColor
      }
      if ($PSBoundParameters.ContainsKey('WDBuiltinStyle')) 
      {
        $WordDoc.application.selection.Style = $WDBuiltinStyle
      }
           
      $WordDoc.Application.Selection.TypeText("$($text)")    
      $WordDoc.Application.Selection.TypeParagraph() 
      $WordDoc.application.selection.Style = [Microsoft.Office.Interop.Word.WdBuiltinStyle]'wdStyleNormal'
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordBreak 
{


  [CmdletBinding( SupportsShouldProcess = $true)]
  param (
    [Parameter(Position = 0, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'GridTable')]
    [ValidateSet('NewPage', 'Section','Paragraph')]
    [string]$breaktype,
    [Parameter(Mandatory = $false)]
    [alias("Word")]
    #Todo cast type instead ie [Microsoft.Office.Interop.Word.Application]$WordInstance but does not work
    [ValidateScript( { if($_ -is [Microsoft.Office.Interop.Word.Application]) { $True } else { throw "WordInstance was not of type [Microsoft.Office.Interop.Word.Application]" } })]
    $WordInstance = $Global:WordInstance,
    [Parameter(Mandatory = $false)]
    $WordDoc =$Global:WordDoc
  )
 
  Begin { Add-Type -AssemblyName Microsoft.Office.Interop.Word
  Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
 
    try
    {  
      switch ($breaktype)
      {
        'NewPage' 
        {
          $WordInstance.Selection.InsertNewPage() 
        }
        'Section' 
        {
          $WordInstance.Selection.Sections.Add()  
        }
        'Paragraph' 
        {
          $WordInstance.Selection.InsertParagraph() 
        }
      }
      [Void]$WordDoc.application.selection.goto([Microsoft.Office.Interop.Word.WdGoToItem]::wdGoToBookmark,$null,$null,'\EndOfDoc')
    
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Set-WordBuiltInProperty 
{
 
  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    
    [Parameter(Position = 0,HelpMessage = 'Add help message for user',Mandatory = $true)] 
    [Microsoft.Office.Interop.Word.WdBuiltInProperty]$WdBuiltInProperty,
    
    [Parameter(Position = 1,HelpMessage = 'Add help message for user',mandatory = $true)] 
    [String]$text,
    
    [Parameter(Mandatory = $false)]
    $WordDoc =$Global:WordDoc
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
 
    try
    { 
      Write-Verbose -Message $WdBuiltInProperty
      $WordDoc.BuiltInDocumentProperties.item($WdBuiltInProperty).value = $text
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordCoverPage 
{
 
  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Position = 0)] 
    [ValidateSet('Austin', 'Banded','Facet','Filigree','Grid','Integral','Ion (Dark)','Ion (Light)','Motion','Retrospect','Semaphore','Sideline','Slice (Dark)','Slice (Light)','Viewmaster','Whisp')]  
    [string]$CoverPage,
    [Parameter(Mandatory = $false)]
    [alias("Word")]
    #Todo cast type instead ie [Microsoft.Office.Interop.Word.Application]$WordInstance but does not work
    [ValidateScript( { if($_ -is [Microsoft.Office.Interop.Word.Application]) { $True } else { throw "WordInstance was not of type [Microsoft.Office.Interop.Word.Application]" } })]
    $WordInstance = $Global:WordInstance,
    [Parameter(Mandatory = $false)]
    $WordDoc =$Global:WordDoc
  )  
  Begin { Add-Type -AssemblyName Microsoft.Office.Interop.Word
  Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 

    try
    {
      $Selection = $WordDoc.application.selection
    
      $WordInstance.Templates.LoadBuildingBlocks()
      $bb = $WordInstance.templates | Where-Object -Property name -EQ -Value 'Built-In Building Blocks.dotx'
      $part = $bb.BuildingBlockEntries.item($CoverPage)
      $null = $part.Insert($WordInstance.Selection.range,$true) 
  
      [Void]$Selection.goto([Microsoft.Office.Interop.Word.WdGoToItem]::wdGoToBookmark,$null,$null,'\EndOfDoc')
    }
    catch
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Set-WordOrientation
{
  
  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Position = 0,HelpMessage = 'Add help message for user',Mandatory = $true)] 
    [ValidateSet('Portrait', 'Landscape')]  
    [string]$Orientation,
    [Parameter(Mandatory = $false)]
    [alias("Word")]
    $WordInstance = $Global:WordInstance
            
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
 
    try 
    {
      switch ($Orientation)
      {
        'Portrait'  
        {
          $WordInstance.Selection.PageSetup.Orientation = 0 
        }
        'Landscape'  
        {
          $WordInstance.Selection.PageSetup.Orientation = 1 
        }    
      }
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordTOC 
{
  
  [CmdletBinding( SupportsShouldProcess = $true)]  
  param (
    [Parameter(Mandatory = $false)]
    [alias("Word")]
    #Todo cast type instead ie [Microsoft.Office.Interop.Word.Application]$WordInstance but does not work
    [ValidateScript( { if($_ -is [Microsoft.Office.Interop.Word.Application]) { $True } else { throw "WordInstance was not of type [Microsoft.Office.Interop.Word.Application]" } })]
    $WordInstance = $Global:WordInstance,
    [Parameter(Mandatory = $false)]
    $WordDoc =$Global:WordDoc
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
   
    try 
    {
      $toc = $WordDoc.TablesOfContents.Add($WordInstance.selection.Range)
      $toc.TabLeader = 0
      $toc.HeadingStyles 
      $WordDoc.Application.Selection.TypeParagraph()
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Update-WordTOC 
{
 
  [CmdletBinding( SupportsShouldProcess = $true)]   
  param (
    [Parameter(Mandatory = $false)]
    $WordDoc =$Global:WordDoc
  )
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 

    try 
    {
      $WordDoc.Fields | 
      ForEach-Object -Process {
        $_.Update() 
      }
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Add-WordTable 
{
  
  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Position = 0,HelpMessage = 'psobject to send to word', Mandatory = $true, ValuefromPipeline = $true)]    
    [psobject]$Object,
    [Parameter(Position = 1)] 
    [Microsoft.Office.Interop.Word.WdAutoFitBehavior]$WdAutoFitBehavior = 'wdAutoFitContent',
    [Parameter(Position = 2)] 
    [Microsoft.Office.Interop.Word.WdDefaultTableBehavior]$WdDefaultTableBehavior = 'wdWord9TableBehavior', 
    [Parameter(Position = 3)]
    [bool]$HeaderRow = $true,
    [bool]$TotalRow = $false,
    [bool]$BandedRow = $true,
    [bool]$FirstColumn = $false,
    [bool]$LastColumn = $false,
    [bool]$BandedColumn = $false,
    [Parameter(Position = 4, Mandatory = $false)] 

    [Parameter(ParameterSetName = 'WDTableFormat')]
    [Microsoft.Office.Interop.Word.WdTableFormat]$WDTableFormat,
    
    [Parameter(Position = 4, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'PlainTable')]
    [validateSet('Table Grid', 'Table Grid Light','Plain Table 1','Plain Table 2','Plain Table 3','Plain Table 4','Plain Table 5')]
    [STRING]$PlainTable,
    [Parameter(Position = 5, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'GridTable')]
    [ValidateSet('Grid Table 1 Light', 'Grid Table 2','Grid Table 3','Grid Table 4','Grid Table 5 Dark','Grid Table 6 Colorful','Grid Table 7 Colorful')]
    [string]$GridTable,
    [Parameter(Position = 6, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'ListTable')]
    [ValidateSet('List Table 1 Light', 'List Table 2','List Table 3','List Table 4','List Table 5 Dark','List Table 6 Colorful','List Table 7 Colorful')]
    [string]$ListTable,
    [Parameter(Position = 7, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'ListTable')]
    [ValidateSet('Accent 1', 'Accent 2','Accent 3','Accent 4','Accent 5','Accent 6')]
    [string]$ListAccent,
    [Parameter(Position = 8, Mandatory = $false)] 
    [Parameter(ParameterSetName = 'GridTable')]
    [ValidateSet('Accent 1', 'Accent 2','Accent 3','Accent 4','Accent 5','Accent 6')]
    [string]$GridAccent,
    [switch]$RemoveProperties,
    [switch]$VerticleTable,
    [switch]$NoParagraph,
    [Parameter(Mandatory = $false)]
    $WordDoc = $Global:WordDoc
  )
   
  Begin { Add-Type -AssemblyName Microsoft.Office.Interop.Word
  Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
    try 
    {
      $TableRange = $WordDoc.application.selection.range
      
      if (!($VerticleTable)) {
        $Columns = @($Object | Get-Member -MemberType Property, NoteProperty).count
        if($RemoveProperties) { $Rows = @($Object).count } 
        else {$Rows = @($Object).count +1 }
      }
      if ($VerticleTable) {
        if($RemoveProperties) { $Columns = @($Object).count } 
        else {$Columns = @($Object).count +1 }
        $Rows = @($Object | Get-Member -MemberType Property, NoteProperty).count
      }
 
      $Table = $WordDoc.Tables.Add($TableRange, $Rows, $Columns,$WdDefaultTableBehavior,$WdAutoFitBehavior) 
      if ($PSBoundParameters.ContainsKey('WDTableFormat')){ $Table.autoformat([Microsoft.Office.Interop.Word.WdTableFormat]::$WDTableFormat) }  
      if ($PSBoundParameters.ContainsKey('PlainTable')){ $Table.style = $PlainTable } 
      if ($PSBoundParameters.ContainsKey('GridTable')) { 
        if($PSBoundParameters.ContainsKey('GridAccent'))
        {
          $Table.style = ($GridTable + ' - ' + $GridAccent) 
        }
        else 
        {
          $Table.style = $GridTable 
        } 
      } 
      if ($PSBoundParameters.ContainsKey('ListTable')) {
        if($PSBoundParameters.ContainsKey('ListAccent'))
        {$Table.style = ($ListTable + ' - ' + $ListAccent)}
        else 
        {$Table.style = $ListTable } 
      }  
      if ($PSBoundParameters.ContainsKey('HeaderRow'))    
      {
        if ($HeaderRow) 
        { $Table.ApplyStyleHeadingRows = $true }
        else 
        {$Table.ApplyStyleHeadingRows = $false } 
      }
      if ($PSBoundParameters.ContainsKey('TotalRow'))     
      {
        if ($TotalRow) 
        {
          $Table.ApplyStyleLastRow = $true 
        }
        else 
        {
          $Table.ApplyStyleLastRow = $false
        } 
      }
      if ($PSBoundParameters.ContainsKey('BandedRow'))    
      {
        if ($BandedRow) 
        {
          $Table.ApplyStyleRowBands = $true 
        }
        else 
        {
          $Table.ApplyStyleRowBands = $false
        } 
      }
      if ($PSBoundParameters.ContainsKey('FirstColumn'))  
      {
        if ($FirstColumn) 
        {
          $Table.ApplyStyleFirstColumn = $true 
        }
        else 
        {
          $Table.ApplyStyleFirstColumn = $false
        } 
      }
      if ($PSBoundParameters.ContainsKey('LastColumn'))   
      {
        if ($LastColumn) 
        {
          $Table.ApplyStyleLastColumn = $true 
        }
        else 
        {
          $Table.ApplyStyleLastColumn = $false
        } 
      }
      if ($PSBoundParameters.ContainsKey('BandedColumn')) 
      {
        if($BandedColumn) 
        {
          $Table.ApplyStyleColumnBands = $true 
        }
        else 
        {
          $Table.ApplyStyleColumnBands = $false
        } 
      }

      [int]$Row = 1
      [int]$Col = 1
    
      $PropertyNames = @()

    
      if ($Object -is [Array]){[ARRAY]$HeaderNames = $Object[0].psobject.properties | ForEach-Object -Process { $_.Name }} 
      else { [ARRAY]$HeaderNames = $Object.psobject.properties | ForEach-Object -Process { $_.Name } }
   
      if($RemoveProperties) { $Table.ApplyStyleHeadingRows = $false }
     
      if (!($VerticleTable)) {
        for ($i = 0; $i -le $Columns -1; $i++) 
        {
          $PropertyNames += $HeaderNames[$i]
          if(!$RemoveProperties) 
          {
            $Table.Cell($Row,$Col).Range.Text = $HeaderNames[$i]
          }
          $Col++
        }    
    
        if(!$RemoveProperties)
        { $Row = 2 }
   
        $Object | 
        ForEach-Object -Process {
          $Col = 1
          for ($i = 0; $i -le $Columns -1; $i++) 
          {      
            $Table.Cell($Row,$Col).Range.Text = (($_."$($PropertyNames[$i])") -as [System.string])
            $Col++
          }    
          $Row++
        }
      } 

      if ($VerticleTable) {
        for ($i = 0; $i -le $Rows -1; $i++) 
        {
          $PropertyNames += $HeaderNames[$i]
          if(!$RemoveProperties) 
          {
            $Table.Cell($Row,$Col).Range.Text = $HeaderNames[$i]
          }
          $Row++
        }    
    
        if(!$RemoveProperties)
        { $Col = 2 }
   
        $Object | 
        ForEach-Object -Process {
          $Row = 1
          for ($i = 0; $i -le $Rows -1; $i++) 
          {      
            $Table.Cell($Row,$Col).Range.Text = (($_."$($PropertyNames[$i])") -as [System.string])
            $Row++
          }    
          $Col++
        }
      }
  
      $Selection = $WordDoc.application.selection
      [Void]$Selection.goto([Microsoft.Office.Interop.Word.WdGoToItem]::wdGoToBookmark,$null,$null,'\EndOfDoc')
      
      if(!($NoParagraph)) { $WordDoc.Application.Selection.TypeParagraph() }
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }

  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Get-WordBuiltinStyle 
{
  [Alias("Get-WordStyles")]  #Do not use this will be depreciated
  [CmdletBinding( SupportsShouldProcess = $true)]
  param()

  Begin { Add-Type -AssemblyName Microsoft.Office.Interop.Word
  Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
  
    try 
    {
      [Enum]::GetNames([Microsoft.Office.Interop.Word.WdBuiltinStyle]) |
      ForEach-Object -Process {[pscustomobject]@{
          Style = $_
      } }
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}

Function Get-WordWdTableFormat 
{
  [CmdletBinding( SupportsShouldProcess = $true)]
  param()

  Begin { Add-Type -AssemblyName Microsoft.Office.Interop.Word
  Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
  
    try 
    {
      [Enum]::GetNames([Microsoft.Office.Interop.Word.WdTableFormat]) |
      ForEach-Object -Process {[pscustomobject]@{
          Style = $_
      } }
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}


Function Add-WordTemplate 
{
  [CmdletBinding( SupportsShouldProcess = $true)]
  param(
    [Parameter(Mandatory = $true,HelpMessage = 'Add word document or template to import',Position = 0)]
    [ValidateScript({ Test-Path -Path $_ })] 
    [string]$filename,
    [Parameter(Mandatory = $false)]
    $WordDoc =$Global:WordDoc  
  )   
  Begin { Write-Verbose -Message "Start  : $($Myinvocation.InvocationName)" }
  Process { 
    
    try 
    {
      $WordDoc.Application.Selection.InsertFile([ref]($filename))
    }
    catch 
    {
      Write-Warning -Message "$($MyInvocation.InvocationName) - $($_.exception.message)"
    }
  }
  End { Write-Verbose -Message "End    : $($Myinvocation.InvocationName)" }
}


Export-ModuleMember -Function * -Alias * -Variable *